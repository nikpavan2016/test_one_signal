/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, ScrollView, ActivityIndicator, KeyboardAvoidingView, TextInput, Image, Button } from 'react-native';
import OneSignal from 'react-native-onesignal';

const imageUri = 'https://cdn-images-1.medium.com/max/300/1*7xHdCFeYfD8zrIivMiQcCQ.png';

export default class App extends Component {
  constructor(properties) {
    super(properties);
    this.state = {};
    this.onReceived = this.onReceived.bind(this);
    this.onOpened = this.onOpened.bind(this);
    this.onIds = this.onIds.bind(this);


    OneSignal.init("979c1015-1431-4d36-a6ad-5c6045f14f00", { kOSSettingsKeyAutoPrompt: true });
  }

  componentDidMount() {
    OneSignal.init("979c1015-1431-4d36-a6ad-5c6045f14f00");

    OneSignal.addEventListener('received', this.onReceived);
    OneSignal.addEventListener('opened', this.onOpened);
    OneSignal.addEventListener('ids', this.onIds);
    OneSignal.addEventListener('emailSubscription', this.onEmailRegistrationChange);
  }

  componentWillUnmount() {
    OneSignal.removeEventListener('received', this.onReceived);
    OneSignal.removeEventListener('opened', this.onOpened);
    OneSignal.removeEventListener('ids', this.onIds);
    OneSignal.removeEventListener('emailSubscription', this.onEmailRegistrationChange);
  }



  onReceived(notification) {
    console.log("Notification received: ", notification);
  }

  onOpened(openResult) {
    console.log('Message: ', openResult.notification.payload.body);
    console.log('Data: ', openResult.notification.payload.additionalData);
    console.log('isActive: ', openResult.notification.isAppInFocus);
    console.log('openResult: ', openResult);
  }

  onIds(device) {
    console.log('Device info: ', device);
  }

  render() {
    return (
      <View>
        <Text> React Native</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: '#F5FCFF'
  },
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
    marginHorizontal: 10
  },
  jsonDebugLabelText: {
    textAlign: 'left',
    color: '#333333',
    marginBottom: 5,
    marginHorizontal: 10
  },
  buttonContainer: {
    flexDirection: 'row',
    overflow: 'hidden',
    borderRadius: 10,
    marginVertical: 10,
    marginHorizontal: 10,
    backgroundColor: "#d45653"
  },
  button: {
    color: '#000000',
    flex: 1
  },
  imageStyle: {
    height: 200,
    width: 200,
    marginTop: 20
  },
  textInput: {
    marginHorizontal: 10,
    height: 40
  }
});